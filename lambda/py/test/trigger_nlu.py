import subprocess
import json
import time
import sys

# Fire request
output = subprocess.check_output("ask api evaluate-nlu -a 09be58bb-8039-4234-b1c5-ea26e8c534b9 -s amzn1.ask.skill.1b638ed4-1603-4a41-9bcc-30d705a3e682 -l en-AU", shell=True)
result_json = json.loads(output)
id = result_json['id']
print("ID '{}' returned".format(id))

completed = False
# TODO - DOES NOT DETECT AN ERROR IN EXECUTION
while not completed:
    try:
        check_complete = subprocess.check_output("ask api get-nlu-evaluation-results -s amzn1.ask.skill.1b638ed4-1603-4a41-9bcc-30d705a3e682 -e {}".format(id), shell=True)
        completed = True
        output_json = json.loads(check_complete)
        print(output_json)
        fail_count = output_json['totalFailed']
        sys.exit(fail_count)

    except subprocess.CalledProcessError:
        time.sleep(3)
