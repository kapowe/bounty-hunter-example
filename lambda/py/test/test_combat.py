import unittest
import logging
import sys
import json
import copy
import random

from alexa import util, data, weapons, ships, systems, combat_engine

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
log = logging.getLogger(__name__)

class TestCorvetteCombat(unittest.TestCase):

    def setUp(self):
        self.attacker = ships.Corvette("Corsair")
        self.defender = ships.Corvette("Meridian")
        self.attacker.launch()
        self.defender.launch()


    def test_corvette_on_corvette_hit_chance(self):
        '''
        TODO - this is a magic number test - please remove
        Verify base hit chance for Corvette is 75% for another Corvette
        '''
        self.assertEqual(combat_engine.get_base_hit_chance(self.attacker, self.defender), 75)


    def test_corvette_destroyed_in_three_hits(self):
        '''
        A Corvette should be destroyed in three standard hits by another Corvette
        '''
        combat_engine.hit_target(self.attacker, self.attacker.weapons[weapons.MASS_DRIVER_ID], self.defender)
        combat_engine.hit_target(self.attacker, self.attacker.weapons[weapons.MASS_DRIVER_ID], self.defender)
        combat_engine.hit_target(self.attacker, self.attacker.weapons[weapons.MASS_DRIVER_ID], self.defender)
        self.assertEqual(self.defender.state, ships.STATE_DESTROYED)


    def test_evasive_maneuvers_applied(self):
        '''
        If both ships are performing evasive maneuvers, chance to hit is looooow...
        TODO - this is another magic number test currently - tuning combat model will break this test
        '''
        self.attacker.state = ships.STATE_EVASIVE_MANEUVERS
        self.defender.state = ships.STATE_EVASIVE_MANEUVERS
        self.assertEqual(combat_engine.get_chance_to_hit(self.attacker, self.defender), 15)


    def test_damaging_system(self):
        '''
        Hitting a system should change its state to damaged
        '''
        combat_engine.hit_target(
                self.attacker,
                self.attacker.weapons[weapons.MASS_DRIVER_ID],
                self.defender,
                self.defender.systems[ships.SYSTEM_IMPULSE_ENGINES])
        self.assertEqual(self.defender.systems[ships.SYSTEM_IMPULSE_ENGINES].state, systems.STATE_DAMAGED)


    def test_destroy_system(self):
        '''
        Hitting a system twice should change its state to destroyed
        '''
        combat_engine.hit_target(
                self.attacker,
                self.attacker.weapons[weapons.MASS_DRIVER_ID],
                self.defender,
                self.defender.systems[ships.SYSTEM_IMPULSE_ENGINES])
        combat_engine.hit_target(
                self.attacker,
                self.attacker.weapons[weapons.MASS_DRIVER_ID],
                self.defender,
                self.defender.systems[ships.SYSTEM_IMPULSE_ENGINES])
        self.assertEqual(self.defender.systems[ships.SYSTEM_IMPULSE_ENGINES].state, systems.STATE_DESTROYED)


    def test_seed_consistent(self):
        '''
        Setting the random seed should produce a consistent result
        '''
        random.seed(20)
        self.assertEqual(93, random.randint(1, 100))
        random.seed(20)
        self.assertEqual(93, random.randint(1, 100))
        random.seed(20)
        self.assertEqual(93, random.randint(1, 100))


    def test_seed_miss(self):
        '''
        Setting the random seed can cause a consistent miss
        '''
        random.seed(20)
        self.assertFalse(combat_engine.fire_at_target(
                self.attacker,
                self.attacker.weapons[weapons.MASS_DRIVER_ID],
                self.defender))
        random.seed(20)
        self.assertFalse(combat_engine.fire_at_target(
                self.attacker,
                self.attacker.weapons[weapons.MASS_DRIVER_ID],
                self.defender))
        random.seed(20)
        self.assertFalse(combat_engine.fire_at_target(
                self.attacker,
                self.attacker.weapons[weapons.MASS_DRIVER_ID],
                self.defender))


    def test_destroy_system(self):
        '''
        Hitting a system twice should change its state to destroyed
        '''
        combat_engine.hit_target(
                self.attacker,
                self.attacker.weapons[weapons.MASS_DRIVER_ID],
                self.defender,
                self.defender.systems[ships.SYSTEM_IMPULSE_ENGINES])
        combat_engine.hit_target(
                self.attacker,
                self.attacker.weapons[weapons.MASS_DRIVER_ID],
                self.defender,
                self.defender.systems[ships.SYSTEM_IMPULSE_ENGINES])
        self.assertEqual(self.defender.systems[ships.SYSTEM_IMPULSE_ENGINES].state, systems.STATE_DESTROYED)


    def test_ion_nets_send_system_offline(self):
        '''
        Hitting a system with ion nets should send it offline in one shot (rather than being destroyed)
        '''
        self.attacker.weapons[weapons.ION_NET_ID] = weapons.IonNet()
        combat_engine.hit_target(
                self.attacker,
                self.attacker.weapons[weapons.ION_NET_ID],
                self.defender,
                self.defender.systems[ships.SYSTEM_IMPULSE_ENGINES])
        self.assertEqual(self.defender.systems[ships.SYSTEM_IMPULSE_ENGINES].state, systems.STATE_OFFLINE)


    def test_create_combat_event(self):
        '''
        Should be able to create a basic combat event
        '''
        jump_drive_online = combat_engine.CombatEvent(combat_engine.EVENT_TYPE_SHIP_JUMP, self.defender)


    def test_initialise(self):
        '''
        Initialising combat should create a new queue of events
        '''
        combat_engine.initialise_combat()
        self.assertIsNotNone(combat_engine._EVENT_QUEUE)
        self.assertEqual(len(combat_engine._UNRESOLVED_DECISIONS), 0)


    def test_jump_engine_fires_in_three_ticks(self):
        '''
        A ship should be able to jump in three combat ticks
        '''
        combat_engine.initialise_combat()
        combat_engine.activate_jump_drives(self.defender)
        self.assertEqual(len(combat_engine.tick()), 0)
        self.assertEqual(len(combat_engine.tick()), 0)
        self.assertEqual(len(combat_engine.tick()), 1)


    def test_call_for_surrender_winning(self):
        '''
        Calling for opponent's surrender when clearly winning should work
        '''
        self.defender.hull_strength[ships.HULL_ALL] = 1
        (did_surrender, radio_response) = data.get_call_for_surrender_response(self.attacker, self.defender)
        self.assertTrue(did_surrender)
        self.assertEqual(radio_response, data.CALL_FOR_SURRENDER_DIRE)


    def test_call_for_surrender_equal(self):
        '''
        Calling for opponent's surrender when battle is matched (both taken damage) should not work
        '''
        combat_engine.initialise_combat()
        self.attacker.hull_strength[ships.HULL_ALL] = 2
        self.defender.hull_strength[ships.HULL_ALL] = 2
        (did_surrender, radio_response) = data.get_call_for_surrender_response(self.attacker, self.defender)
        self.assertFalse(did_surrender)
        self.assertEqual(radio_response, data.CALL_FOR_SURRENDER_CEASEFIRE)


    def test_call_for_surrender_losing(self):
        '''
        Calling for opponent's surrender when clearly losing should not work
        '''
        self.attacker.hull_strength[ships.HULL_ALL] = 1
        (did_surrender, radio_response) = data.get_call_for_surrender_response(self.attacker, self.defender)
        self.assertFalse(did_surrender)
        self.assertEqual(radio_response, data.CALL_FOR_SURRENDER_CONFIDENT)


    def test_weapons_lock_successful(self):
        '''
        Successful weapons lock should change ship state
        '''
        random.seed(1)
        self.assertTrue(combat_engine.acquire_weapons_lock(self.attacker, self.defender))
        self.assertEqual(self.attacker.weapons_lock, self.defender.name)


    def test_weapons_lock_unsuccessful(self):
        '''
        Unsuccessful weapons lock attempt should have no change in ship state
        '''
        random.seed(19)
        self.assertFalse(combat_engine.acquire_weapons_lock(self.attacker, self.defender))
        self.assertIsNone(self.attacker.weapons_lock)
