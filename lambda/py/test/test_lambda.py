from lambda_local.main import call
from lambda_local.context import Context

import lambda_function
import json
import unittest
import logging
import sys
import copy
import random

from alexa import util, systems, weapons, ships, data

MERIDIAN_SHIP_NAME = "Meridian"

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
log = logging.getLogger(__name__)

def call_lambda_with_file(filepath):
    with open(filepath) as json_file:
        data = json.load(json_file)
        return call_lambda_with_json(data)

def call_lambda_with_json(json):
    context = Context(5)
    return call(lambda_function.handler, json, context)

class TestLambda(unittest.TestCase):

    def test_launch_no_error(self):
        '''
        Test launching skill with no error
        '''
        (payload, error) = call_lambda_with_file('test/lambda_payloads/launch.json')
        self.assertIsNone(error)


class TestFireWeapon(unittest.TestCase):

    def test_fire_miss_target(self):
        '''
        Test firing on target and missing
        '''
        with open('test/lambda_payloads/fire_request.json') as json_file:
            fire_json = json.load(json_file)

        fire_json["session"]["attributes"]["seed"] = 20
        (payload, error) = call_lambda_with_json(fire_json)
        self.assertIn(data.FIRE_MISS, payload["response"]["outputSpeech"]["ssml"])

    def test_fire_hit_ion_net(self):
        '''
        Test firing on target with ion net
        '''
        with open('test/lambda_payloads/fire_weapon_system_target.json') as json_file:
            fire_json = json.load(json_file)

        fire_json["session"]["attributes"]["seed"] = 666
        (payload, error) = call_lambda_with_json(fire_json)
        self.assertIn(data.FIRE_HIT, payload["response"]["outputSpeech"]["ssml"])

    def test_fire_hit_target(self):
        '''
        Test firing on target and successfully hitting
        '''
        with open('test/lambda_payloads/fire_request.json') as json_file:
            fire_json = json.load(json_file)

        fire_json["session"]["attributes"]["seed"] = 666
        (payload, error) = call_lambda_with_json(fire_json)
        self.assertIn(data.FIRE_HIT, payload["response"]["outputSpeech"]["ssml"])

    def test_firing_get_hit_light_damage(self):
        '''
        Firing on target should cause them to fire back first time, and score light damage
        '''
        with open('test/lambda_payloads/fire_request.json') as json_file:
            fire_json = json.load(json_file)

        fire_json["session"]["attributes"]["seed"] = 8
        (payload, error) = call_lambda_with_json(fire_json)
        expected_result = "We've sustained light hull damage"
        self.assertIn(expected_result, payload["response"]["outputSpeech"]["ssml"])


class TestScanIntent(unittest.TestCase):

    def test_scan_target_default_state(self):
        '''
        Test scanning a target in default state
        '''
        with open('test/lambda_payloads/scan_request.json') as json_file:
            scan_json = json.load(json_file)

        (payload, error) = call_lambda_with_json(scan_json)
        expected_result = "Meridian is a Corvette class ship. No hull damage detected. Ship is armed with a Mass Driver Cannon and a Missile Pod"
        self.assertIn(expected_result, payload["response"]["outputSpeech"]["ssml"])

    def test_scan_critical_hull_damage(self):
        '''
        Test scanning a target in default state
        '''
        with open('test/lambda_payloads/scan_request.json') as json_file:
            scan_json = json.load(json_file)

        scan_json["session"]["attributes"]["meridian"]["hull_strength"]["all"] = 1
        (payload, error) = call_lambda_with_json(scan_json)
        expected_result = "Meridian is a Corvette class ship. Ship has sustained critical hull damage. Ship is armed with a Mass Driver Cannon and a Missile Pod"
        self.assertIn(expected_result, payload["response"]["outputSpeech"]["ssml"])

    def test_scan_one_system_offline(self):
        '''
        Test scanning a target in default state
        '''
        with open('test/lambda_payloads/scan_request.json') as json_file:
            scan_json = json.load(json_file)

        scan_json["session"]["attributes"]["meridian"]["systems"]["thrusters"]["state"] = 4
        (payload, error) = call_lambda_with_json(scan_json)
        expected_result = "Meridian is a Corvette class ship. No hull damage detected. Ship is armed with a Mass Driver Cannon and a Missile Pod Weapons, Communications, Jump Drive and Sensors detected as online.  Impulse Engines detected as offline."
        self.assertIn(expected_result, payload["response"]["outputSpeech"]["ssml"])

    def test_scan_one_system_offline_one_destroyed(self):
        '''
        Test scanning a target in default state
        '''
        with open('test/lambda_payloads/scan_request.json') as json_file:
            scan_json = json.load(json_file)

        scan_json["session"]["attributes"]["meridian"]["systems"]["thrusters"]["state"] = 4
        scan_json["session"]["attributes"]["meridian"]["systems"]["sensors"]["state"] = 3
        (payload, error) = call_lambda_with_json(scan_json)
        expected_result = "Meridian is a Corvette class ship. No hull damage detected. Ship is armed with a Mass Driver Cannon and a Missile Pod Weapons, Communications and Jump Drive detected as online.  Sensors detected as destroyed.  Impulse Engines detected as offline."
        self.assertIn(expected_result, payload["response"]["outputSpeech"]["ssml"])

    def test_scan_one_system_offline_one_destroyed_one_initialising(self):
        '''
        Test scanning a target in default state
        '''
        with open('test/lambda_payloads/scan_request.json') as json_file:
            scan_json = json.load(json_file)

        scan_json["session"]["attributes"]["meridian"]["systems"]["thrusters"]["state"] = 4
        scan_json["session"]["attributes"]["meridian"]["systems"]["sensors"]["state"] = 3
        scan_json["session"]["attributes"]["meridian"]["systems"]["jump_drive"]["state"] = 5
        (payload, error) = call_lambda_with_json(scan_json)
        expected_result = "Meridian is a Corvette class ship. No hull damage detected. Ship is armed with a Mass Driver Cannon and a Missile Pod Weapons and Communications detected as online.  Sensors detected as destroyed.  Impulse Engines detected as offline.  Jump Drive detected as initialising."
        self.assertIn(expected_result, payload["response"]["outputSpeech"]["ssml"])

    def test_scan_ship_destroyed(self):
        '''
        Test scanning a target in default state
        '''
        with open('test/lambda_payloads/scan_request.json') as json_file:
            scan_json = json.load(json_file)

        scan_json["session"]["attributes"]["meridian"]["state"] = -1
        (payload, error) = call_lambda_with_json(scan_json)
        expected_result = "The Meridian has been destroyed"
        self.assertIn(expected_result, payload["response"]["outputSpeech"]["ssml"])

    def test_scan_sensors_offline(self):
        '''
        Attempting to scan with sensors in a state other than online should give a short error
        '''
        with open('test/lambda_payloads/scan_request.json') as json_file:
            scan_json = json.load(json_file)

        scan_json["session"]["attributes"]["player_ship"]["systems"]["sensors"]["state"] = 4
        (payload, error) = call_lambda_with_json(scan_json)
        expected_result = "Unable to perform scan. Sensors are currently offline."
        self.assertIn(expected_result, payload["response"]["outputSpeech"]["ssml"])


class TestTargetIntent(unittest.TestCase):

    def test_target_meridian_impulse_engines(self):
        '''
        Attempting to target engines should resolve to 'impulse engines' and give response
        '''
        with open('test/lambda_payloads/target_request.json') as json_file:
            scan_json = json.load(json_file)

        (payload, error) = call_lambda_with_json(scan_json)
        expected_result = "Now targeting impulse engines on the meridian"
        self.assertIn(expected_result, payload["response"]["outputSpeech"]["ssml"])

    def test_target_meridian_no_systems(self):
        '''
        Attempting to target just a ship should be successful
        '''
        with open('test/lambda_payloads/target_just_ship.json') as json_file:
            scan_json = json.load(json_file)

        (payload, error) = call_lambda_with_json(scan_json)
        expected_result = "Now targeting the meridian"
        self.assertIn(expected_result, payload["response"]["outputSpeech"]["ssml"])

    def test_just_system(self):
        '''
        Attempting to target just a system with one ship as a target should set the ship as a target as well
        '''
        with open('test/lambda_payloads/target_just_system.json') as json_file:
            scan_json = json.load(json_file)

        (payload, error) = call_lambda_with_json(scan_json)
        expected_result = "Now targeting impulse engines on the meridian"
        self.assertIn(expected_result, payload["response"]["outputSpeech"]["ssml"])


class TestEvasiveManeuvers(unittest.TestCase):

    def test_evasive_maneuvers(self):
        '''
        Attempting to perform evasive maneuvers with default status should be successful
        '''
        with open('test/lambda_payloads/evasive_maneuvers_request.json') as json_file:
            scan_json = json.load(json_file)

        (payload, error) = call_lambda_with_json(scan_json)
        expected_result = data.EVASIVE_MANEUVERS_PERFORMING.strip()
        self.assertIn(expected_result, payload["response"]["outputSpeech"]["ssml"])

    def test_evasive_maneuvers_offline(self):
        '''
        Attempting to perform evasive maneuvers with engines offline gives appropriate feedback
        '''
        with open('test/lambda_payloads/evasive_maneuvers_request.json') as json_file:
            scan_json = json.load(json_file)

        scan_json["session"]["attributes"]["player_ship"]["systems"]["thrusters"]["state"] = systems.STATE_OFFLINE
        (payload, error) = call_lambda_with_json(scan_json)
        expected_result = "Unable to perform evasive manuevers. Engines are currently offline."
        self.assertIn(expected_result, payload["response"]["outputSpeech"]["ssml"])


class TestStatusReport(unittest.TestCase):

    def test_status_report_default_state(self):
        '''
        Status report for ship in default state should be 'all clear'
        '''
        with open('test/lambda_payloads/status_report_request.json') as json_file:
            scan_json = json.load(json_file)

        (payload, error) = call_lambda_with_json(scan_json)
        expected_result = data.STATUS_REPORT_NOTHING.strip()
        self.assertIn(expected_result, payload["response"]["outputSpeech"]["ssml"])

    def test_weapons_lock_intent(self):
        '''
        Basic test for weapons lock
        '''
        with open('test/lambda_payloads/weapons_lock_request.json') as json_file:
            scan_json = json.load(json_file)

        (payload, error) = call_lambda_with_json(scan_json)
        expected_result = "WeaponsLockIntent triggered successfully"
        self.assertIn(expected_result, payload["response"]["outputSpeech"]["ssml"])


class TestTacticalAnalysis(unittest.TestCase):

    def test_tactical_analysis_intent(self):
        '''
        Basic test for tactical analysis
        '''
        with open('test/lambda_payloads/tactical_analysis_request.json') as json_file:
            tac_json = json.load(json_file)

        (payload, error) = call_lambda_with_json(tac_json)
        expected_result = data.TACTICAL_INITIAL_TARGET_ENGINES.format(MERIDIAN_SHIP_NAME)
        self.assertIn(expected_result, payload["response"]["outputSpeech"]["ssml"])

    def test_tactical_analysis_target_panicked(self):
        '''
        Once target has been roughed up a little, (and we're OK) analysis should be
        to give them a few more shots
        '''
        with open('test/lambda_payloads/tactical_analysis_request.json') as json_file:
            tac_json = json.load(json_file)

        tac_json["session"]["attributes"]["meridian"]["hull_strength"]["all"] = 2
        (payload, error) = call_lambda_with_json(tac_json)
        expected_result = data.TACTICAL_INITIAL_JUST_A_FEW_MORE.format(MERIDIAN_SHIP_NAME)
        self.assertIn(expected_result, payload["response"]["outputSpeech"]["ssml"])

    def test_tactical_analysis_target_dire(self):
        '''
        Once target is almost destroyed, recommendation should be to get their
        surrender straight away
        '''
        with open('test/lambda_payloads/tactical_analysis_request.json') as json_file:
            tac_json = json.load(json_file)

        tac_json["session"]["attributes"]["meridian"]["hull_strength"]["all"] = 1
        (payload, error) = call_lambda_with_json(tac_json)
        expected_result = data.TACTICAL_INITIAL_CALL_FOR_SURRENDER.format(MERIDIAN_SHIP_NAME)
        self.assertIn(expected_result, payload["response"]["outputSpeech"]["ssml"])

    def test_tactical_analysis_us_light_target_undamaged(self):
        '''
        Basic test for tactical analysis
        '''
        with open('test/lambda_payloads/tactical_analysis_request.json') as json_file:
            tac_json = json.load(json_file)

        tac_json["session"]["attributes"]["player_ship"]["hull_strength"]["all"] = 2
        (payload, error) = call_lambda_with_json(tac_json)
        expected_result = data.TACTICAL_LIGHT_TARGET_ENGINES.format(MERIDIAN_SHIP_NAME).strip()
        self.assertIn(expected_result, payload["response"]["outputSpeech"]["ssml"])

    def test_tactical_analysis_us_light_target_panicked(self):
        '''
        Once target has been roughed up a little, (and we're OK) analysis should be
        to give them a few more shots
        '''
        with open('test/lambda_payloads/tactical_analysis_request.json') as json_file:
            tac_json = json.load(json_file)

        tac_json["session"]["attributes"]["player_ship"]["hull_strength"]["all"] = 2
        tac_json["session"]["attributes"]["meridian"]["hull_strength"]["all"] = 2
        (payload, error) = call_lambda_with_json(tac_json)
        expected_result = data.TACTICAL_LIGHT_JUST_A_FEW_MORE.format(MERIDIAN_SHIP_NAME)
        self.assertIn(expected_result, payload["response"]["outputSpeech"]["ssml"])

    def test_tactical_analysis_us_light_target_dire(self):
        '''
        Once target is almost destroyed, recommendation should be to get their
        surrender straight away
        '''
        with open('test/lambda_payloads/tactical_analysis_request.json') as json_file:
            tac_json = json.load(json_file)

        tac_json["session"]["attributes"]["player_ship"]["hull_strength"]["all"] = 2
        tac_json["session"]["attributes"]["meridian"]["hull_strength"]["all"] = 1
        (payload, error) = call_lambda_with_json(tac_json)
        expected_result = data.TACTICAL_LIGHT_CALL_FOR_SURRENDER
        self.assertIn(expected_result, payload["response"]["outputSpeech"]["ssml"])

    def test_tactical_analysis_us_dire(self):
        '''
        Once target is almost destroyed, recommendation should be to get their
        surrender straight away
        '''
        with open('test/lambda_payloads/tactical_analysis_request.json') as json_file:
            tac_json = json.load(json_file)

        tac_json["session"]["attributes"]["player_ship"]["hull_strength"]["all"] = 1
        tac_json["session"]["attributes"]["meridian"]["hull_strength"]["all"] = 1
        (payload, error) = call_lambda_with_json(tac_json)
        expected_result = data.TACTICAL_SCRAM
        self.assertIn(expected_result, payload["response"]["outputSpeech"]["ssml"])


class TestOpenCommsIntent(unittest.TestCase):

    def test_open_channel_intent(self):
        '''
        Basic test for opening a channel
        '''
        with open('test/lambda_payloads/open_channel_request.json') as json_file:
            scan_json = json.load(json_file)

        (payload, error) = call_lambda_with_json(scan_json)
        expected_result = data.CALL_FOR_SURRENDER_CONFIDENT
        self.assertIn(expected_result, payload["response"]["outputSpeech"]["ssml"])

    def test_call_for_surrender_winning(self):
        '''
        Call for surrender while clearly winning
        '''
        with open('test/lambda_payloads/open_channel_request.json') as json_file:
            comms_json = json.load(json_file)

        comms_json["session"]["attributes"]["meridian"]["hull_strength"]["all"] = 1
        (payload, error) = call_lambda_with_json(comms_json)
        expected_result = data.CALL_FOR_SURRENDER_DIRE
        self.assertIn(expected_result, payload["response"]["outputSpeech"]["ssml"])


class TestWeaponsLockIntent(unittest.TestCase):

    def test_weapons_lock_successful_no_existing(self):
        '''
        Create a new weapons lock with no previously existing lock
        '''
        with open('test/lambda_payloads/weapons_lock_request.json') as json_file:
            lock_json = json.load(json_file)

        lock_json["session"]["attributes"]["seed"] = 1
        (payload, error) = call_lambda_with_json(lock_json)
        expected_result = data.WEAPONS_LOCK_SUCCESSFUL
        self.assertIn(expected_result, payload["response"]["outputSpeech"]["ssml"])


    def test_weapons_lock_unsuccessful(self):
        '''
        Create a new weapons lock with no previously existing lock
        '''
        with open('test/lambda_payloads/weapons_lock_request.json') as json_file:
            lock_json = json.load(json_file)

        lock_json["session"]["attributes"]["seed"] = 20
        (payload, error) = call_lambda_with_json(lock_json)
        expected_result = data.WEAPONS_LOCK_UNSUCCESSFUL
        self.assertIn(expected_result, payload["response"]["outputSpeech"]["ssml"])


    def test_weapons_lock_cant_find_ship(self):
        '''
        Try to get a weapons lock on a ship that's not there
        '''
        with open('test/lambda_payloads/weapons_lock_request.json') as json_file:
            lock_json = json.load(json_file)

        lock_json["request"]["intent"]["slots"]["target"]["value"] = "phantom"
        (payload, error) = call_lambda_with_json(lock_json)
        expected_result = data.WEAPONS_LOCK_NO_SHIP
        self.assertIn(expected_result, payload["response"]["outputSpeech"]["ssml"])


    def test_weapons_lock_releasing(self):
        '''
        Pre-existing weapons lock is released when trying to get a new one
        '''
        with open('test/lambda_payloads/weapons_lock_request.json') as json_file:
            lock_json = json.load(json_file)

        lock_json["session"]["attributes"]["player_ship"]["weapons_lock"] = "phantom"
        (payload, error) = call_lambda_with_json(lock_json)
        expected_result = data.WEAPONS_LOCK_RELEASING.format("phantom")
        self.assertIn(expected_result, payload["response"]["outputSpeech"]["ssml"])
