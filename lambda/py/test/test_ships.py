import unittest
import logging
import sys
import json
import copy

from alexa import util, data, weapons, ships, systems, combat_engine

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
log = logging.getLogger(__name__)

class TestWeapons(unittest.TestCase):

    def test_create_missile_pod(self):
        '''
        Basic test to create a new missile pod
        '''
        pod = weapons.MissilePod()
        self.assertEqual(pod.damage, 1)


class TestShips(unittest.TestCase):

    def test_standard_corvette_two_weapons(self):
        '''
        A standard Corvette is created with two weapons
        '''
        corsair = ships.Corvette()
        self.assertEqual(len(corsair.weapons), 2)


    def test_all_systems_offline(self):
        '''
        A standard Corvette is created with all systems offline
        '''
        corsair = ships.Corvette()
        for ship_system in corsair.systems.values():
            self.assertEqual(ship_system.state, systems.STATE_OFFLINE)


    def test_rehydration_successful(self):
        '''
        A standard Corvette is created with all systems offline
        '''
        corsair = ships.Corvette("Meridian")
        corsair.hull_strength[ships.HULL_ALL] = 1
        ship_dict = corsair.to_dict()
        back_again = ships.get_ship_from_dict(ship_dict)
        self.assertEqual(back_again.hull_strength[ships.HULL_ALL], 1)


    def test_launch_sets_all_systems_online(self):
        '''
        Launching' a ship should turn on all systems
        '''
        corsair = ships.Corvette("Meridian")
        corsair.launch()
        for system in corsair.systems.values():
            self.assertEqual(systems.STATE_ONLINE, system.state)

    def test_data_article_an_apple(self):
        '''
        Correct article for 'apple' should be 'an '
        '''
        self.assertEqual(data.get_article("apple"), "an ")

    def test_data_article_a_newspaper(self):
        '''
        Correct article for 'newspaper' should be 'a '
        '''
        self.assertEqual(data.get_article("newspaper"), "a ")

    def test_combined_list_two_items_without_articles(self):
        '''
        Utility method for combining list of two items, not using articles
        should just pop an and in
        '''
        self.assertEqual(data.get_text_for_joined_list(["Cheese", "Ninja"]), "Cheese and Ninja")

    def test_evasive_maneuvers(self):
        '''
        Ship in default state should be able to perform evasive manuevers
        '''
        corsair = ships.Corvette("Meridian")
        corsair.launch()
        self.assertTrue(combat_engine.perform_evasive_maneuvers(corsair))

    def test_evasive_maneuvers_offline(self):
        '''
        Ship in offline state should not be able to perform evasive manuevers
        '''
        corsair = ships.Corvette("Meridian")
        self.assertFalse(combat_engine.perform_evasive_maneuvers(corsair))

    def test_status_report_normal_conditions(self):
        '''
        Ship in normal state should give a 'nothing to report' status report
        '''
        corsair = ships.Corvette("Meridian")
        corsair.launch()
        self.assertEqual(data.STATUS_REPORT_NOTHING, data.get_status_report(corsair))

    def test_status_report_minimal_damage(self):
        '''
        Ship with minimal hull damage should report damage
        '''
        corsair = ships.Corvette("Meridian")
        corsair.hull_strength[ships.HULL_ALL] = 2
        corsair.launch()
        expected_result = "We've sustained light hull damage. All systems online and functioning as expected. "
        self.assertEqual(expected_result, data.get_status_report(corsair))

    def test_status_report_engines_offline(self):
        '''
        Ship with engines offline should report system offline
        '''
        corsair = ships.Corvette("Meridian")
        corsair.launch()
        corsair.systems[ships.SYSTEM_IMPULSE_ENGINES].state = systems.STATE_OFFLINE
        expected_result = "No hull damage detected.  Weapons, Communications, Jump Drive and Sensors online.  Impulse Engines offline. "
        self.assertEqual(expected_result, data.get_status_report(corsair))

    def test_systems_in_state_online(self):
        '''
        Ship with all systems online should show all online
        '''
        corsair = ships.Corvette("Meridian")
        corsair.launch()
        self.assertEqual(5, corsair.systems_in_state(systems.STATE_ONLINE))

    def test_pilot_confident_in_default_state(self):
        '''
        Enemy pilot should be confident if ship in default state
        '''
        corsair = ships.Corvette("Meridian")
        corsair.launch()
        self.assertEqual(ships.ATTITUDE_CONFIDENT, corsair.get_pilot_attitude())

    def test_critical_damage_dire_damage(self):
        '''
        Enemy pilot should consider state dire if critical damage sustained
        '''
        corsair = ships.Corvette("Meridian")
        corsair.launch()
        corsair.hull_strength[ships.HULL_ALL] = 1
        self.assertEqual(ships.ATTITUDE_DIRE, corsair.get_pilot_attitude())

    def test_critical_damage_dire_engines_offline(self):
        '''
        Enemy pilot should consider state dire if engines are offline - sitting duck
        '''
        corsair = ships.Corvette("Meridian")
        corsair.launch()
        corsair.systems[ships.SYSTEM_IMPULSE_ENGINES].state = systems.STATE_OFFLINE
        self.assertEqual(ships.ATTITUDE_DIRE, corsair.get_pilot_attitude())

    def test_panicking_light_hull_damage(self):
        '''
        Enemy pilot should panic if light hull damage sustained
        '''
        corsair = ships.Corvette("Meridian")
        corsair.launch()
        corsair.hull_strength[ships.HULL_ALL] = 2
        self.assertEqual(ships.ATTITUDE_PANICKING, corsair.get_pilot_attitude())

    def test_panicking_system_offline(self):
        '''
        Enemy pilot should panic if a system other than engines goes offline
        '''
        corsair = ships.Corvette("Meridian")
        corsair.launch()
        corsair.systems[ships.SYSTEM_SENSORS].state = systems.STATE_OFFLINE
        self.assertEqual(ships.ATTITUDE_PANICKING, corsair.get_pilot_attitude())
