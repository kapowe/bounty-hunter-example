# -*- coding: utf-8 -*-

# This sample demonstrates handling intents from an Alexa skill using the Alexa Skills Kit SDK for Python.
# Please visit https://alexa.design/cookbook for additional examples on implementing slots, dialog management,
# session persistence, api calls, and more.
# This sample is built using the handler classes approach in skill builder.
import logging
import gettext
import random

from ask_sdk_core.skill_builder import SkillBuilder
from ask_sdk_core.dispatch_components import (
    AbstractRequestHandler, AbstractRequestInterceptor, AbstractExceptionHandler)
import ask_sdk_core.utils as ask_utils
from ask_sdk_core.handler_input import HandlerInput
from ask_sdk_core.dispatch_components import (
    AbstractRequestHandler, AbstractExceptionHandler,
    AbstractResponseInterceptor, AbstractRequestInterceptor)

from ask_sdk_model import Response
from alexa import data, systems, weapons, ships, combat_engine, util

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

# Session ship keys
PLAYER_SHIP_KEY = "player_ship"
OTHER_SHIPS_KEY = "other_ships"
DEFAULT_TARGET_SHIP_KEY = "targeting_ship"
DEFAULT_TARGET_SYSTEM_KEY = "targeting_system"

SIMULATION_UNDERWAY = "simulating"
SIMULATION_SUNSTAR_KEY = "sunstar"
SIMULATION_MERIDIAN_KEY = "meridian"

# Slot names
SLOT_WEAPON = "weapon"
SLOT_SYSTEM = "system"
SLOT_TARGET = "target"
SLOT_MESSAGE = "message"


class LaunchRequestHandler(AbstractRequestHandler):
    """Handler for Skill Launch."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool

        return ask_utils.is_request_type("LaunchRequest")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        return StartCombatSimulationIntentHandler().handle(handler_input)


class StartCombatSimulationIntentHandler(AbstractRequestHandler):
    """Handler for starting combat simulation."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("StartCombatSimulationIntent")(handler_input)

    def handle(self, handler_input):
        logger.info("StartCombatSimulationIntentHandler BEGIN")

        _ = handler_input.attributes_manager.request_attributes["_"]
        sunstar = ships.Corvette("Sunstar")
        sunstar.weapons[weapons.ION_NET_ID] = weapons.IonNet()

        sunstar.launch()
        meridian = ships.Corvette("Meridian")
        meridian.launch()
        session_attr = handler_input.attributes_manager.session_attributes
        session_attr[PLAYER_SHIP_KEY] = sunstar.to_dict()
        session_attr[OTHER_SHIPS_KEY] = meridian.name.lower()
        session_attr[meridian.name.lower()] = meridian.to_dict()
        session_attr[SIMULATION_UNDERWAY] = "True"

        speak_output = _(data.COMBAT_SIMULATION_MESSAGE)

        logger.info("StartCombatSimulationIntentHandler END")

        return (
            handler_input.response_builder
            .speak(speak_output)
            .ask(data.FURTHER_ORDERS)
            .response
        )


class FireWeaponIntentHandler(AbstractRequestHandler):
    """Handler for firing a weapon"""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("FireWeaponIntent")(handler_input)

    def handle(self, handler_input):
        logger.info("FireWeaponIntentHandler BEGIN")

        _ = handler_input.attributes_manager.request_attributes["_"]

        weapon = util.get_resolved_value(
            handler_input.request_envelope.request, SLOT_WEAPON)
        session_attr = handler_input.attributes_manager.session_attributes
        target_name = util.get_resolved_value(
            handler_input.request_envelope.request, SLOT_TARGET)
        system = util.get_resolved_value(
            handler_input.request_envelope.request, SLOT_SYSTEM)

        # Get all ships from session
        session_attr = handler_input.attributes_manager.session_attributes
        player_ship = ships.get_ship_from_dict(session_attr[PLAYER_SHIP_KEY])
        other_ship_list = session_attr[OTHER_SHIPS_KEY]
        other_ships = {}
        for ship_id in other_ship_list.split(","):
            logger.debug("Rehydrating ship from ID '{}'".format(ship_id))
            other_ships[ship_id] = ships.get_ship_from_dict(session_attr[ship_id])

        # find or default weapon system
        if weapon is None:
            if weapons.MASS_DRIVER_ID in player_ship.weapons.keys():
                weapon = player_ship.weapons[weapons.MASS_DRIVER_ID]
            else:
                weapon = list(player_ship.weapons.values())[0]
        elif "mass driver" in weapon.lower():
            weapon = player_ship.weapons[weapons.MASS_DRIVER_ID]
        elif "ion net" in weapon.lower():
            weapon = player_ship.weapons[weapons.ION_NET_ID]
        elif "missile" in weapons.lower():
            weapon = player_ship.weapons[weapons.MISSILE_POD_ID]

        # find or default target
        if target_name is None:
            target = list(other_ships.values())[0]
        else:
            target = other_ships[target_name]

        # actual underlying combat engine stuff begin...
        speak_output = _(data.get_firing_bark(weapon.weapon_id, system))

        if combat_engine.fire_at_target(player_ship, weapon, target):
            speak_output = speak_output + _(data.FIRE_HIT)
            if weapon.weapon_id == weapons.ION_NET_ID:
                speak_output = speak_output + " {} are now offline. ".format(system)
            if target.state == ships.STATE_DESTROYED:
                 speak_output = speak_output + _(data.TARGET_DESTROYED)
        else:
            speak_output = speak_output + _(data.FIRE_MISS)

        speak_output = process_combat_tick(player_ship, other_ships, speak_output)
        # actual underlying combat engine stuff end

        # Put all the toys back again
        logger.debug("Storing player ship in session")
        session_attr[PLAYER_SHIP_KEY] = player_ship.to_dict()
        for ship_id, ship in other_ships.items():
            logger.debug("Storing ship with ID '{}' in session".format(ship_id))
            session_attr[ship_id] = ship.to_dict()

        logger.info("FireWeaponIntentHandler END")

        return (
            handler_input.response_builder
            .speak(speak_output)
            .ask(data.FURTHER_ORDERS)
            .response
        )


def process_combat_tick(player_ship, other_ships, speak_output):
    # Get opponent attitude (hacking for the moment and assuming a single opponent)
    meridian = list(other_ships.values())[0]
    attitude = meridian.get_pilot_attitude()
    logger.debug("Meridian's attitude is evaluated to '{}'".format(attitude))
    tick_output = ""
    if attitude == ships.ATTITUDE_CONFIDENT or attitude == ships.ATTITUDE_PANICKING:
        tick_output = "{} is firing upon us. ".format(meridian.name)
        if combat_engine.fire_at_target(meridian, meridian.weapons[weapons.MASS_DRIVER_ID], player_ship):
            tick_output = tick_output + " Direct hit. "
            # Hull damage
            hull = player_ship.evaluate_hull_damage()
            if hull == ships.HULL_DAMAGE_NONE:
                tick_output = tick_output + data.SHIP_NO_HULL_DAMAGE
            if hull == ships.HULL_DAMAGE_LIGHT:
                tick_output = tick_output + data.OUR_SHIP_LIGHT_HULL_DAMAGE
            if hull == ships.HULL_DAMAGE_CRITICAL:
                tick_output = tick_output + data.OUR_SHIP_CRITICAL_HULL_DAMAGE
        else:
            tick_output = tick_output + data.FIRE_MISS

    return speak_output + tick_output


class ScanShipIntentHandler(AbstractRequestHandler):
    """Handler for ship attempting to scan another ship"""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("ScanShipIntent")(handler_input)

    def handle(self, handler_input):
        logger.info("ScanShipIntentHandler BEGIN")

        _ = handler_input.attributes_manager.request_attributes["_"]

        session_attr = handler_input.attributes_manager.session_attributes
        target_name = util.get_resolved_value(
            handler_input.request_envelope.request, SLOT_TARGET)

        # Get all ships from session
        player_ship = ships.get_ship_from_dict(session_attr[PLAYER_SHIP_KEY])
        other_ship_list = session_attr[OTHER_SHIPS_KEY]
        other_ships = {}
        for ship_id in other_ship_list.split(","):
            logger.debug("Rehydrating ship from ID '{}'".format(ship_id))
            other_ships[ship_id] = ships.get_ship_from_dict(session_attr[ship_id])

        # @TODO - turn this into helper function
        if target_name is None:
            if len(other_ships) == 1:
                target = list(other_ships.values())[0]
        else:
            for ship_id, possible_target in other_ships.items():
                if ship_id.lower() in target_name:
                    target = possible_target

        if target is None:
            speak_output = data.CANT_FIND_TARGET.format(target_name)
        else:
            logger.debug(data.SCAN_SHIP_ATTEMPTING.format(target.name))
            if combat_engine.scan_ship(player_ship, target):
                speak_output = _(data.get_scan_description(target))
            else:
                state = player_ship.systems[ships.SYSTEM_SENSORS].state
                if state == systems.STATE_DESTROYED:
                    speak_output = data.SCAN_UNABLE_SENSORS_DESTROYED
                else:
                    speak_output = data.SCAN_UNABLE_SENSORS_CURRENTLY.format(
                            data.get_system_status_description(state))

        logger.info("ScanShipIntentHandler END")

        return (
            handler_input.response_builder
            .speak(speak_output)
            .ask(data.FURTHER_ORDERS)
            .response
        )


class TargetIntentHandler(AbstractRequestHandler):
    """Handler for TargetIntent."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("TargetIntent")(handler_input)

    def handle(self, handler_input):
        logger.info("TargetIntentHandler BEGIN")

        _ = handler_input.attributes_manager.request_attributes["_"]

        session_attr = handler_input.attributes_manager.session_attributes
        target_name = util.get_resolved_value(
            handler_input.request_envelope.request, SLOT_TARGET)
        system = util.get_resolved_value(
            handler_input.request_envelope.request, SLOT_SYSTEM)

        # Get all ships from session
        session_attr = handler_input.attributes_manager.session_attributes
        other_ship_list = session_attr[OTHER_SHIPS_KEY]
        other_ships = other_ship_list.split(",")

        default_ship = None
        default_system = None

        # If system specified, and only one target ship, set ship as default target as well
        if (system is not None
                and target_name is None
                and DEFAULT_TARGET_SHIP_KEY not in session_attr
                and len(other_ships) == 1):
            target_name = other_ships[0]

        speak_output = None
        if target_name is not None:
            logger.debug("Target ship specified is '{}'".format(target_name))
            logger.debug("..list of eligible targets is '{}'".format(other_ships))
            if target_name in other_ships:
                session_attr[DEFAULT_TARGET_SHIP_KEY] = target_name
                default_ship = target_name
                if DEFAULT_TARGET_SYSTEM_KEY in session_attr:
                    del session_attr[DEFAULT_TARGET_SYSTEM_KEY] # Targeting a new ship should clear system targets
            else:
                speak_output = data.TARGET_SHIP_CANNOT_FIND
        elif DEFAULT_TARGET_SHIP_KEY in session_attr:
            default_ship = session_attr[DEFAULT_TARGET_SHIP_KEY]

        if system is not None:
            logger.debug("Target system specified is '{}'".format(system))
            if system == "engines":
                system = systems.SYSTEM_NAME_THRUSTERS.lower()
            if system in systems.ALL_SYSTEM_NAME_SLOT_VALUES:
                session_attr[DEFAULT_TARGET_SYSTEM_KEY] = system
                default_system = system
            else:
                speak_output = data.TARGETING_INVALID_SYSTEM
        elif DEFAULT_TARGET_SYSTEM_KEY in session_attr:
            default_system = session_attr[DEFAULT_TARGET_SYSTEM_KEY]

        if speak_output is None:
            if default_ship is not None:
                if default_system is not None:
                    speak_output = data.TARGETING_SYSTEM_ON_SHIP.format(default_system, default_ship)
                else:
                    speak_output = data.TARGETING_SHIP.format(default_ship)
            elif default_system is not None:
                speak_output = data.TARGETING_SYSTEM_NO_SHIP.format(default_system)

        logger.info("TargetIntentHandler END")

        return (
            handler_input.response_builder
            .speak(speak_output)
            .ask(data.FURTHER_ORDERS)
            .response
        )


class EvasiveManeuversIntentHandler(AbstractRequestHandler):
    """Handler for Help Intent."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("EvasiveManeuversIntent")(handler_input)

    def handle(self, handler_input):
        logger.info("EvasiveManeuversIntentHandler BEGIN")

        _ = handler_input.attributes_manager.request_attributes["_"]

        session_attr = handler_input.attributes_manager.session_attributes
        player_ship = ships.get_ship_from_dict(session_attr[PLAYER_SHIP_KEY])

        if combat_engine.perform_evasive_maneuvers(player_ship):
            speak_output = data.EVASIVE_MANEUVERS_PERFORMING
            session_attr[PLAYER_SHIP_KEY] = player_ship.to_dict()
        else:
            speak_output = data.EVASIVE_MANEUVERS_CANNOT.format(
                    data.get_system_status_description(player_ship.systems[ships.SYSTEM_IMPULSE_ENGINES].state))

        logger.info("EvasiveManeuversIntentHandler END")

        return (
            handler_input.response_builder
            .speak(speak_output)
            .ask(data.FURTHER_ORDERS)
            .response
        )


class StatusReportIntentHandler(AbstractRequestHandler):
    """Handler for Hello World Intent."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("StatusReportIntent")(handler_input)

    def handle(self, handler_input):
        logger.info("StatusReportIntentHandler BEGIN")

        _ = handler_input.attributes_manager.request_attributes["_"]

        session_attr = handler_input.attributes_manager.session_attributes
        player_ship = ships.get_ship_from_dict(session_attr[PLAYER_SHIP_KEY])
        speak_output = _(data.get_status_report(player_ship))

        logger.info("StatusReportIntentHandler END")

        return (
            handler_input.response_builder
            .speak(speak_output)
            .ask(_(data.FURTHER_ORDERS))
            .response
        )


class HelloWorldIntentHandler(AbstractRequestHandler):
    """Handler for Hello World Intent."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("HelloWorldIntent")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        _ = handler_input.attributes_manager.request_attributes["_"]
        speak_output = _(data.HELLO_MSG)

        return (
            handler_input.response_builder
            .speak(speak_output)
            # .ask("add a reprompt if you want to keep the session open for the user to respond")
            .response
        )


class WeaponsLockIntentHandler(AbstractRequestHandler):
    """Handler for getting a weapons lock."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("WeaponsLockIntent")(handler_input)

    def handle(self, handler_input):
        logger.info("WeaponsLockIntentHandler START")

        # type: (HandlerInput) -> Response
        _ = handler_input.attributes_manager.request_attributes["_"]
        target_name = util.get_resolved_value(
            handler_input.request_envelope.request, SLOT_TARGET)

        # Get all ships from session
        session_attr = handler_input.attributes_manager.session_attributes
        player_ship = ships.get_ship_from_dict(session_attr[PLAYER_SHIP_KEY])
        other_ship_list = session_attr[OTHER_SHIPS_KEY]
        other_ships = {}
        for ship_id in other_ship_list.split(","):
            logger.debug("Rehydrating ship from ID '{}'".format(ship_id))
            other_ships[ship_id] = ships.get_ship_from_dict(session_attr[ship_id])

        if target_name not in other_ships:
            speak_output = data.WEAPONS_LOCK_NO_SHIP
            logger.info("WeaponsLockIntentHandler END")
            return (
                handler_input.response_builder
                .speak(speak_output)
                .ask(_(data.FURTHER_ORDERS))
                .response
            )

        other_ship = other_ships[target_name]

        speak_output = ""
        if player_ship.weapons_lock is not None:
            speak_output = speak_output + " " + data.WEAPONS_LOCK_RELEASING.format(player_ship.weapons_lock)
            player_ship.weapons_lock = None

        speak_output = speak_output + " " + data.WEAPONS_LOCK_ATTEMPTING.format(target_name)
        if combat_engine.acquire_weapons_lock(player_ship, other_ship):
            speak_output = speak_output + " " + data.WEAPONS_LOCK_SUCCESSFUL
            session_attr[PLAYER_SHIP_KEY] = player_ship.to_dict()
        else:
            speak_output = speak_output + " " + data.WEAPONS_LOCK_UNSUCCESSFUL

        logger.info("WeaponsLockIntentHandler END")
        return (
            handler_input.response_builder
            .speak(speak_output)
            .ask(_(data.FURTHER_ORDERS))
            .response
        )


class TacticalAnalysisIntentHandler(AbstractRequestHandler):
    """Handler for getting tactical analysis."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("TacticalAnalysisIntent")(handler_input)

    def handle(self, handler_input):
        logger.info("TacticalAnalysisIntent START")

        # type: (HandlerInput) -> Response
        _ = handler_input.attributes_manager.request_attributes["_"]

        # Get all ships from session
        session_attr = handler_input.attributes_manager.session_attributes
        player_ship = ships.get_ship_from_dict(session_attr[PLAYER_SHIP_KEY])
        other_ship_list = session_attr[OTHER_SHIPS_KEY]
        other_ships = {}
        for ship_id in other_ship_list.split(","):
            logger.debug("Rehydrating ship from ID '{}'".format(ship_id))
            other_ships[ship_id] = ships.get_ship_from_dict(session_attr[ship_id])
        meridian = other_ships[SIMULATION_MERIDIAN_KEY]

        speak_output = data.get_tactical_analysis(player_ship, meridian)

        logger.info("TacticalAnalysisIntentHandler END")
        return (
            handler_input.response_builder
            .speak(speak_output)
            .ask(_(data.FURTHER_ORDERS))
            .response
        )


class OpenChannelIntentHandler(AbstractRequestHandler):
    """Handler for getting tactical analysis."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("OpenChannelIntent")(handler_input)

    def handle(self, handler_input):
        logger.info("OpenChannelIntentHandler START")

        # type: (HandlerInput) -> Response
        _ = handler_input.attributes_manager.request_attributes["_"]

        message = util.get_resolved_value(
            handler_input.request_envelope.request, SLOT_MESSAGE)

        # Get all ships from session
        session_attr = handler_input.attributes_manager.session_attributes
        player_ship = ships.get_ship_from_dict(session_attr[PLAYER_SHIP_KEY])
        other_ship_list = session_attr[OTHER_SHIPS_KEY]
        other_ships = {}
        for ship_id in other_ship_list.split(","):
            logger.debug("Rehydrating ship from ID '{}'".format(ship_id))
            other_ships[ship_id] = ships.get_ship_from_dict(session_attr[ship_id])

        # Call for their surrender
        their_ship = list(other_ships.values())[0]
        if "call" in message and "their" in message and "surrender" in message:
            speak_output = data.OPENING_CHANNEL_TO_GET_SURRENDER.format(their_ship.name)
            (successful, response) = data.get_call_for_surrender_response(player_ship, their_ship)
            speak_output = speak_output + " I have them on speaker... " + " " + response
            if successful:
                speak_output = speak_output + " " + data.OPPONENT_SURRENDERED
                logger.info("OpenChannelIntentHandler END")
                return (
                    handler_input.response_builder
                    .speak(speak_output)
                    .response
                )
        else:
            speak_output = "I'm not quite sure how to do that, captain."

        logger.info("OpenChannelIntentHandler END")
        return (
            handler_input.response_builder
            .speak(speak_output)
            .ask(_(data.FURTHER_ORDERS))
            .response
        )


class HelpIntentHandler(AbstractRequestHandler):
    """Handler for Help Intent."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_intent_name("AMAZON.HelpIntent")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        _ = handler_input.attributes_manager.request_attributes["_"]
        speak_output = _(data.HELP_MSG)

        return (
            handler_input.response_builder
            .speak(speak_output)
            .ask(speak_output)
            .response
        )


class CancelOrStopIntentHandler(AbstractRequestHandler):
    """Single handler for Cancel and Stop Intent."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return (ask_utils.is_intent_name("AMAZON.CancelIntent")(handler_input) or
                ask_utils.is_intent_name("AMAZON.StopIntent")(handler_input))

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        _ = handler_input.attributes_manager.request_attributes["_"]
        speak_output = _(data.GOODBYE_MSG)

        return (
            handler_input.response_builder
            .speak(speak_output)
            .response
        )


class SessionEndedRequestHandler(AbstractRequestHandler):
    """Handler for Session End."""

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_request_type("SessionEndedRequest")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response

        # Any cleanup logic goes here.

        return handler_input.response_builder.response


class IntentReflectorHandler(AbstractRequestHandler):
    """The intent reflector is used for interaction model testing and debugging.
    It will simply repeat the intent the user said. You can create custom handlers
    for your intents by defining them above, then also adding them to the request
    handler chain below.
    """

    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ask_utils.is_request_type("IntentRequest")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        _ = handler_input.attributes_manager.request_attributes["_"]
        intent_name = ask_utils.get_intent_name(handler_input)
        speak_output = _(data.REFLECTOR_MSG).format(intent_name)

        return (
            handler_input.response_builder
            .speak(speak_output)
            # .ask("add a reprompt if you want to keep the session open for the user to respond")
            .response
        )


class CatchAllExceptionHandler(AbstractExceptionHandler):
    """Generic error handling to capture any syntax or routing errors. If you receive an error
    stating the request handler chain is not found, you have not implemented a handler for
    the intent being invoked or included it in the skill builder below.
    """

    def can_handle(self, handler_input, exception):
        # type: (HandlerInput, Exception) -> bool
        return True

    def handle(self, handler_input, exception):
        # type: (HandlerInput, Exception) -> Response
        logger.error(exception, exc_info=True)
        _ = handler_input.attributes_manager.request_attributes["_"]
        speak_output = _(data.ERROR)

        return (
            handler_input.response_builder
            .speak(speak_output)
            .ask(speak_output)
            .response
        )


class LocalizationInterceptor(AbstractRequestInterceptor):
    """
    Add function to request attributes, that can load locale specific data
    """

    def process(self, handler_input):
        locale = handler_input.request_envelope.request.locale
        i18n = gettext.translation(
            'data', localedir='locales', languages=[locale], fallback=True)
        handler_input.attributes_manager.request_attributes["_"] = i18n.gettext


# Testing tool which checks for seed being set in session
class RandomSeed(AbstractRequestInterceptor):
    """Log the request envelope."""
    def process(self, handler_input):
        # type: (HandlerInput) -> None
        logger.debug("Request Envelope: {}".format(
            handler_input.request_envelope))
        session_attr = handler_input.attributes_manager.session_attributes
        if "seed" in session_attr:
            seed_set = session_attr["seed"]
            logger.debug("Setting seed to {}".format(seed_set))
            random.seed(seed_set)


# Request and Response Loggers
class RequestLogger(AbstractRequestInterceptor):
    """Log the request envelope."""
    def process(self, handler_input):
        # type: (HandlerInput) -> None
        logger.debug("Request Envelope: {}".format(
            handler_input.request_envelope))


class ResponseLogger(AbstractResponseInterceptor):
    """Log the response envelope."""
    def process(self, handler_input, response):
        # type: (HandlerInput, Response) -> None
        logger.debug("Response: {}".format(response))


# The SkillBuilder object acts as the entry point for your skill, routing all request and response
# payloads to the handlers above. Make sure any new handlers or interceptors you've
# defined are included below. The order matters - they're processed top to bottom.


sb = SkillBuilder()

sb.add_request_handler(LaunchRequestHandler())
sb.add_request_handler(FireWeaponIntentHandler())
sb.add_request_handler(WeaponsLockIntentHandler())
sb.add_request_handler(ScanShipIntentHandler())
sb.add_request_handler(TargetIntentHandler())
sb.add_request_handler(StatusReportIntentHandler())
sb.add_request_handler(TacticalAnalysisIntentHandler())
sb.add_request_handler(OpenChannelIntentHandler())
sb.add_request_handler(EvasiveManeuversIntentHandler())
sb.add_request_handler(HelloWorldIntentHandler())
sb.add_request_handler(HelpIntentHandler())
sb.add_request_handler(CancelOrStopIntentHandler())
sb.add_request_handler(SessionEndedRequestHandler())
# make sure IntentReflectorHandler is last so it doesn't override your custom intent handlers
sb.add_request_handler(IntentReflectorHandler())

sb.add_global_request_interceptor(LocalizationInterceptor())
sb.add_global_request_interceptor(RandomSeed())
# sb.add_global_request_interceptor(RequestLogger())
# sb.add_global_response_interceptor(ResponseLogger())

sb.add_exception_handler(CatchAllExceptionHandler())


handler = sb.lambda_handler()
