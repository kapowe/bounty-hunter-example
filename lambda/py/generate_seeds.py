import random

for i in range(1, 100):
    random.seed(i)
    values = []
    for j in range(10):
        values.append(str(random.randint(1, 100)))
    print(str(i) + "|" + ",".join(values))
