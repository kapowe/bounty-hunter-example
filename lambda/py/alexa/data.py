from gettext import gettext as _

from alexa import weapons, ships, systems

OPENING_MESSAGE = _("Welcome, Captain")
WHAT_ARE_YOUR_ORDERS = _("What are your orders, Captain?")
FURTHER_ORDERS = _("Awaiting further orders")

COMBAT_SIMULATION_MESSAGE = _("Welcome to the hotseat, captain. You are piloting the Corvette Sunstar, in combat range of the Corvette Meridian. Strap on your jacket, start giving your crew orders")

# Combat barks
FIRING_MASS_DRIVERS = _("Firing mass drivers at target. ")
FIRING_ION_NET = _("Firing Ion Net, targeting {}. ")
FIRING_MISSILE_POD = _("Launching missile pod volley. ")

FIRE_HIT = _("A solid hit, captain.")
FIRE_MISS = _("No connection, captain. ")
TARGET_DESTROYED = _("Target destroyed! ")

EVASIVE_MANEUVERS_PERFORMING = _("Performing evasive manuevers. ")
EVASIVE_MANEUVERS_CANNOT = _("Unable to perform evasive manuevers. Engines are currently {}. ")

SYSTEM_OFFLINE = _("Target system now offline.")

TARGET_SHIP_CANNOT_FIND = _("Unable to locate a ship with that designation. ")

STATUS_REPORT_NOTHING = _("Nothing to report. No hull damage detected. All systems online and functioning as expected. ")
STATUS_REPORT_SYSTEMS_NORMAL = _("All systems online and functioning as expected. ")
STATUS_REPORT_NO_HULL_DAMAGE = _("No hull damage detected. ")

SHIP_NO_HULL_DAMAGE = "No hull damage detected. "
SHIP_LIGHT_HULL_DAMAGE = "Ship has sustained light hull damage. "
SHIP_CRITICAL_HULL_DAMAGE = "Ship has sustained critical hull damage. "

SHIP_ALL_SYSTEMS_ONLINE = "All systems appear to be online. "

OUR_SHIP_LIGHT_HULL_DAMAGE = "We've sustained light hull damage. "
OUR_SHIP_CRITICAL_HULL_DAMAGE = "We've sustained critical hull damage, captain. "

CANT_FIND_TARGET = "Unable to find target by name {}"
SCAN_SHIP_ATTEMPTING = "Attempting to scan ship {}"
SCAN_UNABLE_SENSORS_DESTROYED = "Unable to perform scan. Sensors have been destroyed."
SCAN_UNABLE_SENSORS_CURRENTLY = "Unable to perform scan. Sensors are currently {}."

TARGETING_INVALID_SYSTEM = "I can't find a system by that name to target. "
TARGETING_SHIP = "Now targeting the {}"
TARGETING_SYSTEM_ON_SHIP = "Now targeting {} on the {}"
TARGETING_SYSTEM_NO_SHIP = "Now targeting {}"

# Tactical responses
TACTICAL_INITIAL_TARGET_ENGINES = "The {} is feeling brassy right now, but if we target their engines with our ion net, that'll make them reconsider quick smart."
TACTICAL_INITIAL_JUST_A_FEW_MORE = "The {} has got to be getting panicky. Another shot should do it, then open comms and get them to stand down."
TACTICAL_INITIAL_CALL_FOR_SURRENDER = "The {} is all but done for. Recommend opening comms and calling for their immediate surrender, then we can make this bloodless."
TACTICAL_LIGHT_TARGET_ENGINES = "We've taken a few scratches, but we're ok. Recommend evasive maneuvers, and taking out engines on the {} with our ion net. "
TACTICAL_LIGHT_JUST_A_FEW_MORE = "We've taken a few scratches, but so has the {}. Recommend evasive maneuvuers, and another shot should do it, then open comms and get them to stand down."
TACTICAL_LIGHT_CALL_FOR_SURRENDER = "We've got a few dings, but the {} is all but done for. Recommend opening comms and calling for their immediate surrender."
TACTICAL_SCRAM = "We're almost done for, captain. Recommend spinning up jump drives and getting the hell out of here."
TACTICAL_NOT_YET_IMPLEMENTED = "Uh, give me a minute Captain. I'm not sure"

OPENING_CHANNEL_TO_GET_SURRENDER = "Opening a channel to {} to call for their surrender."
CALL_FOR_SURRENDER_DIRE = "Dammit... yes. Shutting down engines now. Keeping this channel open to negotiate terms."
CALL_FOR_SURRENDER_CEASEFIRE = "Look, let's stop the shooting, call it a draw, and go our own ways."
CALL_FOR_SURRENDER_CONFIDENT = "You can choke on the black before we'll surrender to you."

WEAPONS_LOCK_RELEASING = "Releasing weapons lock on the {}."
WEAPONS_LOCK_ATTEMPTING = "Attempting to acquire weapons lock on the {}."
WEAPONS_LOCK_SUCCESSFUL = "Weapons lock obtained."
WEAPONS_LOCK_UNSUCCESSFUL = "Weapons lock attempt unsuccessful."
WEAPONS_LOCK_NO_SHIP = "I can't find a ship with that name, Captain."

OPPONENT_SURRENDERED = "Nice work, captain. A bloodless win, and some nice salvage to boot. We'll take it from here"
ERROR = _("Sorry, I had trouble doing what you asked. Please try again.")

"""
Functions for building up complex answers
"""

def get_article(word):
    if word[0] in ['a', 'e', 'i', 'o', 'u']:
        return "an "
    else:
        return "a "

def get_text_for_joined_list(join_list, use_articles=False):

    last_item = join_list.pop()
    list_items = []
    if len(join_list) > 0:
        for list_item in join_list:
            if use_articles:
                list_items.append(get_article(list_item) + list_item)
            else:
                list_items.append(list_item)

        if use_articles:
            described_list = ", ".join(list_items) + " and " + get_article(last_item) + last_item
        else:
            described_list = ", ".join(list_items) + " and " + last_item

        return described_list
    else:
        return last_item

def get_system_status_description(status):
    if status == systems.STATE_ONLINE:
        return "online"
    elif status == systems.STATE_DAMAGED:
        return "damaged"
    elif status == systems.STATE_DESTROYED:
        return "destroyed"
    elif status == systems.STATE_OFFLINE:
        return "offline"
    elif status == systems.STATE_INITIALISING:
        return "initialising"
    elif status == systems.STATE_ACTIVATING:
        return "activating"
    elif status == systems.STATE_INOPERABLE:
        return "inoperable"
    else:
        raise Exception("Unexpected system status code {}".format(status))

def get_firing_bark(weapon_id, system=None):
    if weapon_id == weapons.MASS_DRIVER_ID:
        return _(FIRING_MASS_DRIVERS)
    elif weapon_id == weapons.MISSILE_POD_ID:
        return _(FIRING_MISSILE_POD)
    elif weapon_id == weapons.ION_NET_ID:
        if system is None:
            raise Exception("System required for targeting ion net - none specified")
        return _(FIRING_ION_NET.format(system))
    else:
        raise Exception("Unexpected weapon ID {}".format(weapon_id))

def get_scan_description(ship):
    if ship.state == ships.STATE_DESTROYED:
        scan_output = "The {} has been destroyed.".format(ship.name)
        return scan_output

    scan_output = "{} is a {} class ship. ".format(ship.name, ship.class_designation)

    # Hull damage
    hull = ship.evaluate_hull_damage()
    if hull == ships.HULL_DAMAGE_NONE:
        scan_output = scan_output + SHIP_NO_HULL_DAMAGE
    if hull == ships.HULL_DAMAGE_LIGHT:
        scan_output = scan_output + SHIP_LIGHT_HULL_DAMAGE
    if hull == ships.HULL_DAMAGE_CRITICAL:
        scan_output = scan_output + SHIP_CRITICAL_HULL_DAMAGE

    # Weapons
    weapon_description = "Ship is armed with "
    weapon_descriptions = []
    for weapon in ship.weapons.values():
        weapon_descriptions.append(get_article(weapon.name) + weapon.name)

    last_weapon = weapon_descriptions.pop()
    weapon_description = weapon_description + ", ".join(weapon_descriptions) + " and " + last_weapon

    scan_output += weapon_description

    # Systems
    system_statuses = set([system.state for system in ship.systems.values()])
    if len(system_statuses) == 1 and systems.STATE_ONLINE in system_statuses:
        scan_output = scan_output + " " + SHIP_ALL_SYSTEMS_ONLINE
    else:
        system_output = ""
        for status in system_statuses:
            systems_with_status = [this_system.name for this_system in ship.systems.values() if this_system.state == status]
            system_output = system_output + " {} detected as {}. ".format(
                    get_text_for_joined_list(systems_with_status),
                    get_system_status_description(status))
        scan_output = scan_output + system_output

    return scan_output

def get_status_report(ship):

    hull_damage_report = None

    # Hull damage
    hull = ship.evaluate_hull_damage()
    if hull == ships.HULL_DAMAGE_LIGHT:
        hull_damage_report = OUR_SHIP_LIGHT_HULL_DAMAGE
    if hull == ships.HULL_DAMAGE_CRITICAL:
        hull_damage_report = OUR_SHIP_CRITICAL_HULL_DAMAGE

    # Systems
    system_statuses = set([system.state for system in ship.systems.values()])
    if len(system_statuses) == 1 and systems.STATE_ONLINE in system_statuses:
        system_output = None
    else:
        # @TODO - special case - "all systems" for all states
        system_output = ""
        for status in system_statuses:
            systems_with_status = [this_system.name for this_system in ship.systems.values() if this_system.state == status]
            system_output = system_output + " {} {}. ".format(
                    get_text_for_joined_list(systems_with_status),
                    get_system_status_description(status))

    if hull_damage_report is None and system_output is None:
        return STATUS_REPORT_NOTHING
    elif hull_damage_report is not None and system_output is None:
        return hull_damage_report + STATUS_REPORT_SYSTEMS_NORMAL
    elif system_output is not None and hull_damage_report is None:
        return STATUS_REPORT_NO_HULL_DAMAGE + system_output
    else:
        return hull_damage_report + system_output

def get_tactical_analysis(our_ship, their_ship):
    """
    Gets recommendation on tactical situation from 2IC
    """
    our_ship_damage = our_ship.evaluate_hull_damage()
    if our_ship_damage == ships.HULL_DAMAGE_NONE:
        if their_ship.get_pilot_attitude() == ships.ATTITUDE_CONFIDENT:
            return TACTICAL_INITIAL_TARGET_ENGINES.format(their_ship.name)
        elif their_ship.get_pilot_attitude() == ships.ATTITUDE_PANICKING:
            return TACTICAL_INITIAL_JUST_A_FEW_MORE.format(their_ship.name)
        elif their_ship.get_pilot_attitude() == ships.ATTITUDE_DIRE:
            return TACTICAL_INITIAL_CALL_FOR_SURRENDER.format(their_ship.name)
    elif our_ship_damage == ships.HULL_DAMAGE_LIGHT:
        if their_ship.get_pilot_attitude() == ships.ATTITUDE_CONFIDENT:
            return TACTICAL_LIGHT_TARGET_ENGINES.format(their_ship.name)
        elif their_ship.get_pilot_attitude() == ships.ATTITUDE_PANICKING:
            return TACTICAL_LIGHT_JUST_A_FEW_MORE.format(their_ship.name)
        elif their_ship.get_pilot_attitude() == ships.ATTITUDE_DIRE:
            return TACTICAL_LIGHT_CALL_FOR_SURRENDER
    else:
        return TACTICAL_SCRAM

    return TACTICAL_NOT_YET_IMPLEMENTED

def get_call_for_surrender_response(our_ship, their_ship):
    """
    Gets the response to calling for a ship's surrender, including both whether
    the ship responds, and the text response
    """
    our_ship_damage = our_ship.evaluate_hull_damage()
    their_pilot_attitude = their_ship.get_pilot_attitude()

    if their_pilot_attitude == ships.ATTITUDE_DIRE:
        return (True, CALL_FOR_SURRENDER_DIRE)
    elif their_pilot_attitude == ships.ATTITUDE_PANICKING:
        if our_ship_damage != ships.HULL_DAMAGE_NONE:
            return (False, CALL_FOR_SURRENDER_CEASEFIRE)
        else:
            return (True, CALL_FOR_SURRENDER_DIRE)
    elif their_pilot_attitude == ships.ATTITUDE_CONFIDENT:
        return (False, CALL_FOR_SURRENDER_CONFIDENT)
