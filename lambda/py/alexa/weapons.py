import logging
import sys

from alexa import data
from statemachine.exceptions import TransitionNotAllowed

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
log = logging.getLogger(__name__)

UNDEFINED = -1

# Mass Driver stats
MASS_DRIVER_ID = "massdriver"
MASS_DRIVER_DAMAGE = 1
MASS_DRIVER_NAME = "Mass Driver Cannon"

# Missile Pod stats
MISSILE_POD_ID = "missilepod"
MISSILE_POD_DAMAGE = 1
MISSILE_POD_NAME = "Missile Pod"

# Ion Net stats
ION_NET_ID = "ionnet"
ION_NET_DAMAGE = 0
ION_NET_NAME = "Ion Net"


def get_weapon_from_dict(weapon_dict):
    if weapon_dict['weapon_id'] == MASS_DRIVER_ID:
        weapon = MassDriver()
    elif weapon_dict['weapon_id'] == MISSILE_POD_ID:
        weapon = MissilePod()
    elif weapon_dict['weapon_id'] == ION_NET_ID:
        weapon = IonNet()
    else:
        raise Exception("Unsupported weapon ID '{}'".format(weapon_dict['weapon_id']))
    return weapon

class Weapon:

    weapon_id = None
    name = None
    damage = UNDEFINED
    capacity = UNDEFINED
    current_load = UNDEFINED

class MassDriver(Weapon):

    def __init__(self):
        self.weapon_id = MASS_DRIVER_ID
        self.damage = MASS_DRIVER_DAMAGE
        self.name = MASS_DRIVER_NAME

class MissilePod(Weapon):

    def __init__(self):
        self.weapon_id = MISSILE_POD_ID
        self.damage = MISSILE_POD_DAMAGE
        self.name = MISSILE_POD_NAME

class IonNet(Weapon):

    def __init__(self):
        self.weapon_id = ION_NET_ID
        self.damage = ION_NET_DAMAGE
        self.name = ION_NET_NAME
