import logging
import sys

from alexa import data, weapons
from statemachine.exceptions import TransitionNotAllowed

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
log = logging.getLogger(__name__)

"""
Ship subsystems
"""

EFFICACY_STANDARD = 1

STATE_ONLINE = 1
STATE_DAMAGED = 2
STATE_DESTROYED = 3
STATE_OFFLINE = 4
STATE_INITIALISING = 5
STATE_ACTIVATING = 6
STATE_INOPERABLE = 7


# Printable system names
SYSTEM_NAME_WEAPONS = "Weapons"
SYSTEM_NAME_COMMS = "Communications"
SYSTEM_NAME_THRUSTERS = "Impulse Engines"
SYSTEM_NAME_JUMP_DRIVE = "Jump Drive"
SYSTEM_NAME_SENSORS = "Sensors"

ALL_SYSTEM_NAME_SLOT_VALUES = [
    SYSTEM_NAME_WEAPONS.lower(),
    SYSTEM_NAME_COMMS.lower(),
    SYSTEM_NAME_THRUSTERS.lower(),
    SYSTEM_NAME_SENSORS.lower()
]

def get_system_from_dict(system_dict):

    created_system = None
    system_name = system_dict['name']
    if system_name == SYSTEM_NAME_WEAPONS:
        created_system = Weapons()
    elif system_name == SYSTEM_NAME_COMMS:
        created_system = Communications()
    elif system_name == SYSTEM_NAME_THRUSTERS:
        created_system = ImpulseEngines()
    elif system_name == SYSTEM_NAME_JUMP_DRIVE:
        created_system = JumpDrive()
    elif system_name == SYSTEM_NAME_SENSORS:
        created_system = Sensors()
    else:
        raise Exception("Unsupported system name '{}'".format(system_name))

    created_system.state = system_dict['state']
    return created_system


class System():

    name = None
    efficacy = EFFICACY_STANDARD
    state = STATE_OFFLINE # Don't turn that thing on in here!

    def __init__(self):
        self.state = STATE_OFFLINE


class Weapons(System):

    def __init__(self):
        super().__init__()
        self.name = "Weapons"


class Communications(System):

    def __init__(self):
        super().__init__()
        self.name = "Communications"


class Sensors(System):

    def __init__(self):
        super().__init__()
        self.name = "Sensors"


class ImpulseEngines(System):

    def __init__(self):
        super().__init__()
        self.name = "Impulse Engines"


class JumpDrive(System):

    def __init__(self):
        super().__init__()
        self.name = "Jump Drive"
