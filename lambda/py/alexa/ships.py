import logging
import sys

from alexa import data, weapons, systems
from statemachine.exceptions import TransitionNotAllowed

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
log = logging.getLogger(__name__)

# Keys for use in accessing systems
SYSTEM_WEAPONS = "weapons"
SYSTEM_COMMUNICATIONS = "comms"
SYSTEM_IMPULSE_ENGINES = "thrusters"
SYSTEM_JUMP_DRIVE = "jump_drive"
SYSTEM_SENSORS = "sensors"

# Ship state
STATE_DESTROYED = -1
STATE_DEFAULT = 0
STATE_EVASIVE_MANEUVERS = 2

HULL_ALL = "all"

# Corvette stats
CORVETTE_ID = "corvette" # When referring to Corvette ships/stats
CORVETTE_HULL_STRENGTH = 3

# Hull damage evaluations
HULL_DAMAGE_NONE = 0
HULL_DAMAGE_LIGHT = 1
HULL_DAMAGE_CRITICAL = 2

# Attitudes for enemy pilots
ATTITUDE_CONFIDENT = 1      # Convinced of their own success
ATTITUDE_PANICKING = 2      # Nervous and wanting to get out of dodge
ATTITUDE_DIRE = 3           #

# Dict keys for

def get_ship_from_dict(ship_dict):
    if ship_dict['class'] != 'Corvette':
        log.error("Unsupported ship class '{}'".format(ship_dict['class']))
        return

    ship = Corvette()
    ship.name = ship_dict['name']
    ship.state = ship_dict['state']
    if 'weapons_lock' in ship_dict:
        ship.weapons_lock = ship_dict['weapons_lock']

    # Hull strength
    ship.hull_strength = {}
    for hull_key in ship_dict['hull_strength'].keys():
        ship.hull_strength[hull_key] = ship_dict['hull_strength'][hull_key]

    # Weapons
    for weapon_key in ship_dict['weapons']:
        ship.weapons[weapon_key] = weapons.get_weapon_from_dict(ship_dict['weapons'][weapon_key])


    # Systems
    for system_key in ship_dict['systems']:
        ship.systems[system_key] = systems.get_system_from_dict(ship_dict['systems'][system_key])

    return ship


class Ship:

    name = None
    hull_strength = None
    systems = None
    state = STATE_DEFAULT
    class_designation = None


    def __init__(self, name=None):
        if name is not None:
            self.name = name
        self.state = STATE_DEFAULT
        self.weapons = {}
        self.systems = {}


    def to_dict(self):
        # @TODO - this changes the object the method is called on for some reason, converting it to a dict. Not a *huge* issue for the moment, but to be looked at
        ship_dict = vars(self)

        # Add class name
        ship_dict['class'] = self.__class__.__name__

        # replace weapons
        for weapon_key in ship_dict['weapons'].keys():
            ship_dict['weapons'][weapon_key] = vars(self.weapons[weapon_key])

        # replace systems
        for system_key in ship_dict['systems'].keys():
            ship_dict['systems'][system_key] = vars(self.systems[system_key])

        return ship_dict


    def launch(self):
        for system in self.systems.values():
            system.state = systems.STATE_ONLINE


    def evaluate_hull_damage(self):
        return HULL_DAMAGE_NONE


    def systems_in_state(self, state):
        """
        Returns number of systems in supplied state
        """
        return len([sys for sys in self.systems.values() if sys.state == state])


    def get_pilot_attitude(self):
        if (self.evaluate_hull_damage() == HULL_DAMAGE_CRITICAL
                or self.systems[SYSTEM_IMPULSE_ENGINES].state != systems.STATE_ONLINE
                or self.systems[SYSTEM_WEAPONS].state != systems.STATE_ONLINE):
            return ATTITUDE_DIRE

        if (self.evaluate_hull_damage() == HULL_DAMAGE_LIGHT
                or self.systems_in_state(systems.STATE_OFFLINE) > 0
                or self.systems_in_state(systems.STATE_DAMAGED) > 0):
            return ATTITUDE_PANICKING

        return ATTITUDE_CONFIDENT


class Corvette(Ship):

    class_id = CORVETTE_ID
    name = None
    weapons = None
    hull_strength = None
    weapons_lock = None

    def __init__(self, name=None):
        super().__init__(name)
        self.hull_strength = { HULL_ALL : CORVETTE_HULL_STRENGTH }
        self.class_designation = "Corvette"
        self.weapons_lock = None

        # Initialise subsystems
        self.weapons = {
            weapons.MASS_DRIVER_ID : weapons.MassDriver(),
            weapons.MISSILE_POD_ID : weapons.MissilePod()
        }

        self.systems = {
            SYSTEM_WEAPONS : systems.Weapons(),
            SYSTEM_COMMUNICATIONS : systems.Communications(),
            SYSTEM_IMPULSE_ENGINES : systems.ImpulseEngines(),
            SYSTEM_JUMP_DRIVE : systems.JumpDrive(),
            SYSTEM_SENSORS : systems.Sensors()
        }

    def evaluate_hull_damage(self):
        return (3 - self.hull_strength[HULL_ALL])
