import logging
import sys
import random

from alexa import data, ships, weapons, systems
from statemachine.exceptions import TransitionNotAllowed
from collections import deque

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
log = logging.getLogger(__name__)

"""
Implementation of combat engine rules for bounty hunter experience
"""

# Dictionary of hit chances keyed on class of attacker first, then target
BASE_HIT_CHANCE = {
    ships.CORVETTE_ID : {
        ships.CORVETTE_ID : 75
    }
}

# Hit chance modifiers
HIT_CHANCE_MODIFIER_WEAPONS_LOCK = 25
HIT_CHANCE_MODIFIER_TARGETING_SYSTEM = -50
HIT_CHANCE_MODIFIER_TARGET_EVASIVE_MANEUVERS = -40
HIT_CHANCE_MODIFIER_PERFORMING_EVASIVE_MANUEVERS = -20

WEAPONS_LOCK_BASE_CHANCE = 80

JUMP_DRIVE_ACTIVATION_TIME = 2      # Number of combat ticks for a ships's combat engines to activate

# Queue of combat events
MAX_QUEUE_LENGTH = 5    # Total number of turns ahead to track for events
_EVENT_QUEUE = None
_UNRESOLVED_DECISIONS = None


def get_base_hit_chance(attacker, target):
    """
    Determine the base hit chance for attacking ship to hit target considering
    only the ship classes
    """
    return BASE_HIT_CHANCE[attacker.class_id][target.class_id]


def get_chance_to_hit(attacker, target):
    """
    Calculates the chance to hit specific target for attacking ship
    """
    log.debug("TARGET:\n\n{}".format(target))
    chance_to_hit = BASE_HIT_CHANCE[attacker.class_id][target.class_id]
    if attacker.state == ships.STATE_EVASIVE_MANEUVERS:
        chance_to_hit = chance_to_hit + HIT_CHANCE_MODIFIER_PERFORMING_EVASIVE_MANUEVERS

    if target.state == ships.STATE_EVASIVE_MANEUVERS:
        chance_to_hit = chance_to_hit + HIT_CHANCE_MODIFIER_TARGET_EVASIVE_MANEUVERS

    return chance_to_hit


def fire_at_target(attacker, weapon, target, location=None):
    """
    Attempt to fire at target. Returns whether target was a success
    """
    chance = get_chance_to_hit(attacker, target)
    roll = random.randint(1, 100)
    log.debug("Roll to hit was {} against chance of {}".format(roll, chance))
    if roll <= chance:
        hit_target(attacker, weapon, target, location)
        return True
    else:
        return False


def activate_jump_drives(ship):
    """
    Ship starts to jump
    """
    log.debug("Ship {} is activating jump drive. Will take {} ticks to fire.".format(
            ship.name,
            JUMP_DRIVE_ACTIVATION_TIME
    ))
    ship.systems[ships.SYSTEM_JUMP_DRIVE].state = systems.STATE_ACTIVATING
    ship_jump = CombatEvent(EVENT_TYPE_SHIP_JUMP, ship)
    insert_index = MAX_QUEUE_LENGTH - 1 - JUMP_DRIVE_ACTIVATION_TIME
    log.debug("Inserting at index {}".format(insert_index))
    _EVENT_QUEUE[insert_index].append(ship_jump)


def hit_target(attacker, weapon, target, location=None):
    """
    Apply the effects of target being hit in specified location by the weapon
    used. If no target is specified, a random location on the target's hull
    will be used to apply the damage
    """
    if location is None:
        log.debug("Applying effects - {} has hit {} with {} on random hull location ({} damage)".format(
                attacker.name, target.name, weapon.name, weapon.damage))
        hull_location = random.choice(list(target.hull_strength.keys()))
        target.hull_strength[hull_location] = target.hull_strength[hull_location] - weapon.damage
        if target.hull_strength[hull_location] == 0:
            log.debug("Target {} has been destroyed".format(target.name))
            target.state = ships.STATE_DESTROYED
    else:
        if isinstance(location, systems.System):
            log.debug("Applying effects - {} has hit {}, targeting {}".format(
                    attacker.name,
                    target.name,
                    location.name))
            if isinstance(weapon, weapons.IonNet):
                log.debug("Weapon is {}. Sending targeted system offline".format(weapon.name))
                location.state = systems.STATE_OFFLINE
                return

            if location.state == systems.STATE_DAMAGED:
                # TODO - 'cheat' for player ship - should go to inoperable, not destroyed
                location.state = systems.STATE_DESTROYED
                log.debug("..{} are now damaged".format(location.name))
            else:
                location.state = systems.STATE_DAMAGED
                log.debug("..{} are now destroyed".format(location.name))


def scan_ship(scanner, target):
    """
    Performs checks to ensure that scanner is able to scan target (taking into
    account things like advanced avionics) and returns whether scan was successful
    """
    return scanner.systems[ships.SYSTEM_SENSORS].state == systems.STATE_ONLINE


def perform_evasive_maneuvers(ship):
    """
    Checks to ensure that ship is able to perform evasive maneuvers, and then
    performs appropriate state updates
    """

    if ship.systems[ships.SYSTEM_IMPULSE_ENGINES].state == systems.STATE_ONLINE:
        ship.state = ships.STATE_EVASIVE_MANEUVERS
        return True
    else:
        return False


def acquire_weapons_lock(ship, target):
    """
    Attempt for ship to acquire weapons lock on target. Return whether
    """
    if random.randint(1, 100) <= WEAPONS_LOCK_BASE_CHANCE:
        ship.weapons_lock = target.name
        return True
    else:
        return False


"""
Methods dealing with combat engine state
"""

EVENT_TYPE_SHIP_JUMP = 1


class CombatEvent():

    type = None
    actor = None
    target = None

    def __init__(self, type, actor, target=None):
        self.type = type
        self.actor = actor
        if target is not None:
            self.target = target


def initialise_combat():
    """
    Initialises state of combat engine, ready to track combat events
    """
    global _EVENT_QUEUE
    global _UNRESOLVED_DECISIONS

    _EVENT_QUEUE = ([[] for i in range(MAX_QUEUE_LENGTH)])
    _UNRESOLVED_DECISIONS = ()


def tick():
    """
    Ticks over combat engine and returns events to be processed in current tick
    """
    global _EVENT_QUEUE
    tick_events = _EVENT_QUEUE.pop()
    _EVENT_QUEUE.insert(0, [])

    return tick_events
